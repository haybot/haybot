use anyhow::{Context, Result};
use serenity::{self, framework::StandardFramework, prelude::*};

use crate::server::event_handler::Handler;

pub async fn create_client() -> Result<Client> {
	Client::builder(
		env!(
			"DISCORD_BOT_TOKEN",
			"DISCORD_BOT_TOKEN environment variable isn't set"
		),
		GatewayIntents::non_privileged(),
	)
	.event_handler(Handler)
	.framework(StandardFramework::new())
	.await
	.context("Error while creating client")
}
