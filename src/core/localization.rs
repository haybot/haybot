use std::{
	fmt::{Display, Formatter, Result},
	ops::Deref,
};

use serde::{Deserialize, Serialize};
use serenity::model::timestamp::Timestamp;

use crate::core::location::Location;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Localization {
	Still(Location),
	Moving(Location, Location, Timestamp),
}

impl Display for Localization {
	fn fmt(&self, f: &mut Formatter<'_>) -> Result {
		let now = Timestamp::now();
		match self {
			Self::Moving(from, to, a) if (*a.deref() - *now.deref()).is_positive() => {
				let duration = (*a.deref() - *now.deref()).whole_seconds();
				let (h, m, s) = (duration / 3600, duration % 3600 / 60, duration % 3600 % 60);
				write!(
					f,
					"`{}` :arrow_forward: `{}`\nTemps de trajet restant `{:02}:{:02}:{:02}`",
					from, to, h, m, s
				)
			}
			Self::Still(l) | Self::Moving(_, l, _) => {
				write!(f, "`{}`", l)?;

				if let Ok(loc_data) = l.get_data() {
					write!(f, "{}", loc_data.description)?;
				}

				Ok(())
			}
		}
	}
}
