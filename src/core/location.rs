use std::{
	self,
	collections::HashMap,
	fmt::{Display, Error, Result},
	path::PathBuf,
};

use anyhow::{Context, Result as AnyhowResult};
use enum_iterator::{all, Sequence};
use serde::{Deserialize, Serialize};
use tokio::sync::OnceCell;

use crate::core::path::Path;

#[derive(Clone, Debug, Default, Deserialize, Eq, Hash, PartialEq, Sequence, Serialize)]
pub enum Location {
	#[default]
	AerfordDowntown,
	AerfordGates,
	AerfordMarketplace,
	AerfordPier,
	Elianor,
	Falanor,
	FangdorLeftBank,
	FangdorRightBank,
	HarduinBarracks,
	HarduinForge,
	HarduinGates,
	HarduinStables,
	HimmevelEarthSky,
	HimmevelFoothill,
	HimmevelLevallon,
	KilberryBazaar,
	KilberryBlackMarket,
	KilberryGates,
	LebanorasGates,
	LebanorasHarbor,
	Merino,
	Myrlyl,
	Nandaral,
	NumeriaGates,
	NumeriaHarbor,
	NumeriaLibrary,
	NumeriaUniversity,
	NumeriaWorkshops,
	OrestmarArdres,
	OrestmarPort,
	OrestmarSandmar,
	OrestmarSaragar,
	Rosenholm,
	Salidor,
	Valestal,
}

// Initialized in main() on startup
pub(crate) static LOCATIONS: OnceCell<HashMap<Location, LocationData>> = OnceCell::const_new();

impl Display for Location {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result {
		write!(f, "{}", self.get_data().map_err(|_| Error)?.name)
	}
}

impl Location {
	pub fn get_data(&self) -> AnyhowResult<&'static LocationData> {
		LOCATIONS
			.get()
			.context("Error while getting LOCATIONS content")?
			.get(self)
			.context("Unknown location")
	}

	pub fn init() -> AnyhowResult<()> {
		LOCATIONS
			.set(
				all::<Self>()
					.map(|l| {
						let path = l.resource_path()?;
						let loc_data = ron::de::from_reader(
							std::fs::File::open(&path)
								.with_context(|| format!("Error while opening {:?}", path))?,
						)
						.with_context(|| format!("Error while deserializing {:?}", path))?;

						Ok((l, loc_data))
					})
					.collect::<AnyhowResult<_>>()?,
			)
			.context("Error while initializing LOCATIONS")
	}

	pub(crate) fn resource_path(&self) -> AnyhowResult<PathBuf> {
		Ok(PathBuf::from(env!("LOCATIONS_PATH")).join(
			ron::to_string(&self).with_context(|| format!("Error while serializing {:?}", self))?
				+ ".ron",
		))
	}
}

#[derive(Debug, Deserialize)]
pub struct LocationData {
	pub(crate) name: String,
	pub description: String,
	pub paths: Vec<Path>,
}
