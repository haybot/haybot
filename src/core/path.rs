use std::{
	fmt::{Display, Formatter, Result as FmtResult},
	time::Duration,
};

use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};
use serenity::model::Timestamp;

use crate::{core::location::Location, utils::constants::*};

#[derive(Debug, Deserialize, Serialize)]
pub struct Path {
	pub destination: Location,
	pub duration: Duration,
	pub transport: Transport,
}

impl Path {
	pub fn arrival_time(&self) -> Result<Timestamp> {
		Timestamp::from_unix_timestamp(
			Timestamp::now().unix_timestamp() + self.duration.as_secs() as i64,
		)
		.context("Error while building timestamp")
	}

	pub fn cost(&self) -> u32 {
		match self.transport {
			Transport::Maritime(cost) => cost,
			_ => 0,
		}
	}
}

impl Display for Path {
	fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
		let duration = match self.duration.as_secs_f64() {
			d if d < ONE_HOUR => format!("{}m", d / ONE_MINUTE),
			d => format!("{}h", d / ONE_HOUR),
		};

		write!(f, "{}: {}", self.destination, duration)?;

		if let cost @ 1.. = self.cost() {
			write!(f, " ({} pièces)", cost)?;
		}

		Ok(())
	}
}

#[derive(Debug, Deserialize, Serialize)]
pub enum Transport {
	Pedestrian,
	Maritime(u32),
}
