use anyhow::Error;

#[derive(thiserror::Error, Debug)]
pub enum CmdError {
	#[error(transparent)]
	AfterResponse(Error),
	#[error(transparent)]
	BeforeResponse(#[from] Error),
}

impl CmdError {
	pub fn after_response(e: Error) -> Self {
		Self::AfterResponse(e)
	}
}
